# libraries
import os
import math
import torch
from torch.utils.data import DataLoader

# dependencies from this project's modules
import net
from net import net_to_conv_layers, ConvOnlyNet, Container
from dataset import DatasetVPR

# where to store learned weights
weights_path = "../weights/"

# where to store extracted feature maps
feature_maps_path = "../feature_maps/"

# for data folder, go to dataset.py


def training(db_name, net_name, num_epochs, lr, momentum, weight_decay, scheduler_step, scheduler_gamma, batch_size=64, to_cuda=True):
    name_str = "{}@{}@{}@{}@{}@{}@{}".format(net_name, lr, momentum, weight_decay, scheduler_step, scheduler_gamma, db_name)
    print("Fine-tuning: {}".format(name_str))

    os.system("mkdir -p {}".format(weights_path))

    ds = DatasetVPR(db_name=db_name, in_advance=False, to_cuda=to_cuda)
    dataloader = DataLoader(ds, batch_size=batch_size, shuffle=True)

    model = net.model(net_name, pretrained=True, num_classes=ds.len_D)
    softmax = torch.nn.Softmax(dim=1)

    if to_cuda:
        model = model.to(torch.device("cuda:0"))
        softmax = softmax.to(torch.device("cuda:0"))

    optimizer = torch.optim.SGD(model.parameters(), lr=lr, momentum=momentum, weight_decay=weight_decay)
    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=scheduler_step, gamma=scheduler_gamma)

    for epoch_i in range(num_epochs):
        epoch_loss = 0
        for batch_i, batch in enumerate(dataloader):
            x_batch = batch[0]
            y_batch = batch[1]
            optimizer.zero_grad()
            with torch.set_grad_enabled(True):
                loss = -torch.sum(torch.multiply(y_batch, torch.log(softmax(model(x_batch)))))
                loss.backward()
                optimizer.step()
            epoch_loss += loss.item()
        scheduler.step()

        if math.isnan(epoch_loss):
            print("Optimization went wrong (NaN.")
            exit()
        print("Epoch: {}/{}; loss: {:.5f}".format(epoch_i + 1, num_epochs, epoch_loss))

    os.system("mkdir -p {}".format(weights_path))
    torch.save(model, "{}{}.pt".format(weights_path, name_str))


def extraction(db_name, test_db_name, net_name, lr, momentum, weight_decay, scheduler_step, scheduler_gamma, batch_size=64, to_cuda=True):
    name_str = "{}@{}@{}@{}@{}@{}@{}".format(net_name, lr, momentum, weight_decay, scheduler_step, scheduler_gamma, db_name)
    print("Extraction: {}".format(name_str))

    if lr == 0.0:
        # if learning rate is 0.0 we did not perform fine-tuning at all
        # therefore, pretrained network is used
        model = net.model(net_name)

    elif name_str + ".pt" not in os.listdir(weights_path):
        # else if a network is not found given its fine-tuning params
        # terminate the execution
        print("Not available")
        exit()

    else:
        # everything's fine, load optimized weights
        model = torch.load("{}{}.pt".format(weights_path, name_str))

    # second step in the paper: use only conv. layers in order to extract features
    conv_model = ConvOnlyNet(net_to_conv_layers(model, net_name))
    # we are not optimizing a net anymore
    conv_model.eval()

    if to_cuda:
        conv_model = conv_model.to(torch.device("cuda:0"))

    ds = DatasetVPR(db_name=test_db_name, in_advance=False, to_cuda=to_cuda)
    dataloader = DataLoader(ds, batch_size=batch_size, shuffle=False)

    z = None
    for batch_i, batch in enumerate(dataloader):
        x_batch = batch[0]
        with torch.set_grad_enabled(False):
            z_batch = torch.flatten(conv_model(x_batch), start_dim=1)
            if z is None:
                z = z_batch
            else:
                z = torch.cat((z, z_batch), 0)
    z = z.cpu()
    Q = z[:ds.len_Q]
    D = z[ds.len_Q:]

    # normalize
    Q.div_(torch.norm(Q, 2, 1, True))
    D.div_(torch.norm(D, 2, 1, True))

    if torch.isnan(Q).any() or torch.isnan(D).any():
        print("Something is wrong with either a network or dataset")
        exit()

    # the last part is to store extracted features into appropriate pytorch structures
    # (you can also store Q and D for your purposes, e.g. Q.numpy() to convert Q into numpy matrix...)
    query_db_container = torch.jit.script(Container(Q))
    ref_db_container = torch.jit.script(Container(D))

    # save to a given path
    os.system("mkdir -p {}".format(feature_maps_path))
    torch.jit.save(query_db_container, "{}{}_{}_query.pt".format(feature_maps_path, name_str, test_db_name))
    torch.jit.save(ref_db_container, "{}{}_{}_reference.pt".format(feature_maps_path, name_str, test_db_name))


if __name__ == "__main__":
    db_name = "freiburg"
    test_db_name = "bonn"
    net_name = "resnet50"
    lr = 0.001
    momentum = 0.95
    weight_decay = 0.05
    scheduler_step = 10
    scheduler_gamma = 0.1
    num_epochs = 200

    # to extract feature maps from a pretrained net use
    # extraction(db_name, test_db_name, net_name, 0, 0, 0, 0, 0)
    # otherwise
    training(db_name, net_name, num_epochs, lr, momentum, weight_decay, scheduler_step, scheduler_gamma)
    extraction(db_name, test_db_name, net_name, lr, momentum, weight_decay, scheduler_step, scheduler_gamma)
