from torchvision.models import alexnet, resnet18, resnet50, vgg16
import torch.nn as nn


def model(name, pretrained=True, num_classes=1000):
    model = None
    if name == "alexnet":
        model = alexnet(pretrained=pretrained)
        fc_in_features = model.classifier[1].in_features
        model.classifier = nn.Linear(fc_in_features, num_classes)

    if name == "vgg16":
        model = vgg16(pretrained=True)
        fc_in_features = model.classifier[0].in_features
        model.classifier = nn.Linear(fc_in_features, num_classes)

    elif name == "resnet18":
        model = resnet18(pretrained=pretrained)
        fc_in_features = model.fc.in_features
        model.fc = nn.Linear(fc_in_features, num_classes)

    elif name == "resnet50":
        model = resnet50(pretrained=pretrained)
        fc_in_features = model.fc.in_features
        model.fc = nn.Linear(fc_in_features, num_classes)

    return model


def net_to_conv_layers(model, name):
    if name == "alexnet":
        # 4th convolutional layer
        return list(model.children())[0:-1][0][0:10]

    elif name == "vgg16":
        return list(model.children())[0:-1][0][0:29]

    elif name == "resnet18":
        # 17th convolutional layer
        return list(model.children())[0:-2][0:8]

    elif name == "resnet50":
        # 49th convolutional layer
        return list(model.children())[0:-2][0:8]


class ConvOnlyNet(nn.Module):

    def __init__(self, layers):
        super(ConvOnlyNet, self).__init__()
        self.base = nn.Sequential(*layers)

    def forward(self, x):
        x = self.base(x)
        return x


class Container(nn.Module):
    def __init__(self, t):
        super().__init__()
        self.tensor = nn.Parameter(t)