# Softmax regression for visual place recognition #

### What is this repository for? ###

* This is an initial version of our softmax regression approach for visual place recognition. Here you can either extract feature maps from a pretrained convolutional neural network or to fine-tune a network and then to extract feature maps
* ver 1.0.0.

### How do I get set up? ###

* This project is implemented by using PyTorch (ver. 1.7.1)
* Developed and executed on Ubuntu 20.04 with a CUDA GPU and CUDA 11.1 installed
* Modify `weights_path` and `feature_maps_path` in `main.py` according to your needs
* Modify `data_path` and `DatasetVPR.__init__` in `dataset.py` according to your needs
* For the third part of approach - **sequence-based algorithm**, please confer [NOSeqSLAM implementation page](https://bitbucket.org/unizg-fer-lamor/noseqslam/src/master/)

### Who do I talk to? ###
* If you have a question, please send it to the corresponding author of this paper. 