import os
import torch
from torch.utils.data import Dataset
from torchvision.transforms import transforms
from PIL import Image


# where data reside
data_path = "../data/"

# ordinary transform
transform_ = transforms.Compose(
        [
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize(
                mean=[0.485, 0.456, 0.406],
                std=[0.229, 0.224, 0.225]
            )
        ]
    )


# nordland data already resized and scaled
transform_nordland = transforms.Compose(
    [
        transforms.ToTensor(),
        transforms.Normalize(
            mean=[0.485, 0.456, 0.406],
            std=[0.229, 0.224, 0.225]
        )
    ]
)

class DatasetVPR(Dataset):
    __slots__ = [
        "len_Q", "len_D", "length",
        "filepaths", "transform", "in_advance", "to_cuda",
        "db_name", "y", "X", "gt"
    ]

    @property
    def Q(self):
        return self.X[:self.len_Q]

    @property
    def D(self):
        return self.X[self.len_Q:]

    def __init__(self, db_name, transform=transform_, in_advance=False, to_cuda=False, flatten=False):
        self.db_name = db_name

        if "nordland" in db_name:
            # nordland datasets already resized and cropped
            self.transform = transform_nordland
        else:
            # as usual for images
            self.transform = transform

        # load the data in advance on RAM/GPU
        self.in_advance = in_advance
        # upload on GPU
        self.to_cuda = to_cuda

        # to flat the data
        self.flatten = flatten

        # In the original Bonn dataset (also applies to Freiburg)
        # folder name is: bonn_example
        # gt path and its name is: bonn_example/gt_bonn_example.out
        # query path is: bonn_example/query/images/
        # query path is: bonn_example/reference/images/
        # your data should reside in data_path, therefore in this scenario: "../data/bonn_example/"

        root = "{}{}_example/".format(data_path, db_name)
        query_filepaths = sorted(["{}query/images/".format(root) + filepath for filepath in os.listdir("{}query/images/".format(root))])
        reference_filepaths = sorted(["{}reference/images/".format(root) + filepath for filepath in os.listdir("{}reference/images/".format(root))])

        # additionally, sort Nordland data from reference [35] in the paper
        if "nordland" in db_name:
            query_filepaths = sorted(query_filepaths, key=lambda x: int(x.split("/")[-1].split(".")[0]))
            reference_filepaths = sorted(reference_filepaths, key=lambda x: int(x.split("/")[-1].split(".")[0]))

        # all images filepaths
        self.filepaths = query_filepaths + reference_filepaths
        self.len_Q = len(query_filepaths)
        self.len_D = len(reference_filepaths)
        self.length = self.len_Q + self.len_D

        # parse ground truth
        gt = []
        gt_filepath = root + "gt_{}_example.out".format(db_name)
        with open(gt_filepath) as fl:
            for ln in fl:
                ln = ln.replace("\n", "")
                gts = ln.split(" ")[2:]
                if gts[-1] == "":
                    gts.pop()
                gt.append([int(g) for g in gts])

        image = Image.open(self.filepaths[0])
        if self.transform is not None:
            image = self.transform(image)
        self.X = torch.Tensor(self.length, image.shape[0], image.shape[1], image.shape[2])
        for i, filepath in enumerate(self.filepaths):
            image = Image.open(filepath)
            if self.transform is not None:
                image = self.transform(image)
            self.X[i] = image

        if self.flatten:
            self.X = self.X.flatten(start_dim=1)
            self.X.div_(torch.norm(self.X, 2, 1, True))

        if self.in_advance:
            if self.to_cuda and torch.cuda.is_available():
                self.X = self.X.to(torch.device("cuda:0"))

        y_Q = torch.zeros((self.len_Q, self.len_D))
        for i, gt_line in enumerate(gt):
            for j in gt_line:
                y_Q[i, j] = 1.0
        y_D = torch.eye(self.len_D)
        self.y = torch.cat([y_Q, y_D], dim=0)
        self.y.div_(torch.sum(self.y, dim=0, keepdim=True))

        if self.to_cuda and torch.cuda.is_available():
            self.y = self.y.to(torch.device("cuda:0"))

    def __len__(self):
        return self.length

    def __getitem__(self, item):
        x_i = self.X[item]
        if self.to_cuda and torch.cuda.is_available():
            x_i = x_i.to(torch.device("cuda:0"))
        return x_i, self.y[item]